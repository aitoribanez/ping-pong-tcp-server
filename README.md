# Ping-pong TCP server

## Use
- To raise the server ```node server.js```
- To raise the client ```node client.js```

## Way to try ping-pong
- No message will be output, that is, it was OK ```netcat 127.0.0.1 10000``` You can use telnet as well.
- Type ```{"command":"ping"}```
- The console will return the pong: ```<127.0.0.1:10000> Pong!```

## Docker
- Build the image. -t is to be able to name the image and be able to find it more easily.  ```docker build -t choose .```
- Verify that you added the image correctly: ```docker ps```
- Start the image by being 49160 the port on your machine/outsite the container where the app will work on port 10000 in the docker container: ```docker run -p 49160:10000 -d choose```
- Get the container_id: ```docker ps```
- View container logs: ``` docker logs <container_id>```
-  Prove that everything went well: ```telnet 127.0.0.1 49160```  and send him a ping.

- To start a network with 3 nodes: ```docker-compose up```

## Help
- https://github.com/sebastianseilund/node-json-socket
- https://nodejs.org/en/docs/guides/nodejs-docker-webapp/
- https://blog.codeship.com/using-docker-compose-for-nodejs-development/
- https://www.youtube.com/watch?v=g1R8XoSH5ig&t=7410s
